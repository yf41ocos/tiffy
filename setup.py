from setuptools import setup

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setup(name='tiffy',
      author='Lukas Riedersberger',
      url='https://bitbucket.org/yf41ocos/tiffy',
      install_requires=requirements,
      version='0.1'
)