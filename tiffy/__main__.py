from tiffy.main import run as run_tiffy


def run():
    run_tiffy()


if __name__ == "__main__":
    run()
