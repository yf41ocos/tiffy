from PySide2.QtWidgets import (QLabel, QVBoxLayout, QHBoxLayout, QWidget, QCheckBox)
from PySide2.QtCore import Slot


class AudioStreamWidget(QWidget):
    def __init__(self, converter, stream):
        QWidget.__init__(self)
        from tiffy import probe
        self.stream = stream
        self.layout = QHBoxLayout()
        self.checkbox = QCheckBox()
        self.layout.addWidget(self.checkbox)
        repr = probe.audiostreamrepresentation(stream)
        if len(repr) > 72:
            repr = repr[0:72]+"..."
        element = QLabel(repr)
        self.layout.addWidget(element)
        self.setLayout(self.layout)
        self.converter = converter
        self.checkbox.stateChanged.connect(self.update)
        from PySide2 import QtCore
        self.checkbox.setCheckState(QtCore.Qt.Checked)

    @Slot()
    def update(self):
        self.converter.update_commandline()


class AudioStreamsWidget(QWidget):
    def __init__(self, converter, streams):
        QWidget.__init__(self)
        self.layout = QVBoxLayout()
        self.streams = []
        for stream in streams:
            widget = AudioStreamWidget(converter, stream)
            self.streams.append(widget)
            self.layout.addWidget(widget)
        self.setLayout(self.layout)
