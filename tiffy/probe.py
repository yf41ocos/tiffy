from pymediainfo import MediaInfo


def generatethumbnail():
    pass


def getstreamtimeinseconds(stream):
    if 'tags' in stream and 'DURATION' in stream['tags']:
        timestamp = stream['tags']['DURATION']
        timestamp = timestamp[0:len(timestamp) - 3]
        from datetime import datetime
        from datetime import timedelta
        d = datetime.strptime(timestamp, "%H:%M:%S.%f").time()
        seconds = timedelta(hours=d.hour, minutes=d.minute, seconds=d.second).total_seconds()
        return max(0, int(seconds))
    return 0


def getstreams(videopath):
    import ffmpeg
    probe = ffmpeg.probe(videopath)
    print(probe)
    return probe['streams']


def getvideostreams(streams):
    videostreams = []
    for stream in streams:
        if stream['codec_type'] == 'video':
            videostreams.append(stream)
    return videostreams


def getaudiostreams(streams):
    videostreams = []
    for stream in streams:
        if stream['codec_type'] == 'audio':
            videostreams.append(stream)
    return videostreams


def getstreamindex(stream):
    return stream['index']


def audiostreamrepresentation(stream):
    repr = str(stream['codec_name']) + " " + str(stream['channel_layout']) + " " + str(
        int(float(stream['sample_rate']) / 1000.0)) + " kHz"

    if 'tags' in stream:
        for key in stream['tags']:
            repr = repr + " " + stream['tags'][key]

    return repr


def videostreamrepresentation(stream):
    bits = ''
    if 'bits_per_raw_sample' in stream:
        bits = " " + str(stream['bits_per_raw_sample']) + "bit"
    return str(stream['codec_name']) + " " + str(stream['width']) + "x" + str(stream['height']) + bits


def getAVGFPS(stream):
    framerateformat = str.split(stream['avg_frame_rate'], '/')
    fps = float(framerateformat[0])
    if len(framerateformat) > 1:
        fps = fps / float(framerateformat[1])
    return fps


def printvideostream(stream):
    print(getstreamindex(stream))
    name = stream['codec_name']
    width = stream['width']
    height = stream['height']
    aspect = stream['display_aspect_ratio']
    pixelformat = stream['pix_fmt']
    framerateformat = str.split(stream['avg_frame_rate'], '/')
    fps = float(framerateformat[0])
    if len(framerateformat) > 1:
        fps = fps / float(framerateformat[1])

    shdr = "SDR"

    if 'bits_per_raw_sample' in stream and int(stream['bits_per_raw_sample']) > 9:
        shdr = "HDR"

    # print(name, width, height, aspect, pixelformat, fps, shdr)

    print("Codec:", name)
    print("Resolution:", width, "x", height)
    print("Aspect Ratio:", aspect)
    print("Pixelformat:", pixelformat)
    print("AVG FPS:", fps)
    print("Colorwidth:", shdr)


def getHDRMetaData(path):
    media_info = MediaInfo.parse(path)

    def isfloat(value):
        try:
            float(value)
            return True
        except ValueError:
            return False

    for track in media_info.tracks:
        if track.track_type == 'Video':
            isNone = False
            items = {
                'commercial_name': track.commercial_name,
                'format_profile': str.split(track.format_profile, '@')[1],
                'bit_depth': track.bit_depth,
                'mastering_display_color_primaries': track.mastering_display_color_primaries,
                'mastering_display_luminance': track.mastering_display_luminance
            }
            for key in items:
                if items[key] is None:
                    isNone = True
                    break

            if not isNone:
                minmaxLuminance = items['mastering_display_luminance'] = [int(float(s) / 0.00002) for s in
                                                                          str(track.mastering_display_luminance).split()
                                                                          if (isfloat(s))]

                minLuminance = min(minmaxLuminance[0], minmaxLuminance[1])
                maxLuminance = max(minmaxLuminance[0], minmaxLuminance[1])

                items['mastering_display_luminance'] = [minLuminance, maxLuminance]

                primaries = items['mastering_display_color_primaries']

                if "P3" in primaries:
                    primaries = "G(13250,34500)B(7500,3000)R(34000,16000)WP(15635,16450)L(" + str(
                        maxLuminance) + "," + str(minLuminance) + ")"
                elif "709" in primaries:
                    primaries = "G(15000,30000)B(7500,3000)R(32000,16500)WP(15635,16450)L(" + str(
                        maxLuminance) + "," + str(minLuminance) + ")"
                elif "2020" in primaries:
                    primaries = "G(8500,39850)B(6550,2300)R(35400,14600)WP(15635,16450)L(" + str(
                        maxLuminance) + "," + str(minLuminance) + ")"
                else:
                    primaries = None

                items['master-display'] = primaries

                videostream = getvideostreams(getstreams(path))[0]

                items['color_primaries'] = videostream['color_primaries']
                items['color_transfer'] = videostream['color_transfer']
                items['color_space'] = videostream['color_space']

                return items

    return None
