from PySide2.QtWidgets import (QLabel, QPushButton, QVBoxLayout, QHBoxLayout, QWidget, QCheckBox, QFrame, QLineEdit,
                               QTabWidget)
from PySide2.QtCore import Slot


class TabStream(QTabWidget):
    def __init__(self, converter, videostreams, audiostreams):
        QTabWidget.__init__(self)

        from tiffy.AudioStream import AudioStreamsWidget
        from tiffy.VideoStream import VideoStreamsWidget

        self.videostream = VideoStreamsWidget(converter, videostreams)
        self.audiostream = AudioStreamsWidget(converter, audiostreams)

        self.addTab(self.videostream, "Video")
        self.addTab(self.audiostream, "Audio")
        ##self.tab1UI()
        ##self.tab2UI()
        self.setWindowTitle("Available Streams")
