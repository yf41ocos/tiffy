from PySide2.QtWidgets import (QLabel, QMainWindow, QPushButton, QVBoxLayout, QHBoxLayout, QWidget, QCheckBox, QFrame,
                               QDockWidget, QLineEdit, QProgressBar, QFileDialog)
from PySide2.QtCore import Slot, Qt
from PySide2 import QtCore, QtGui, QtWidgets

import psutil


class DataInfo(QWidget):
    def __init__(self, path, videostreams):
        QWidget.__init__(self)

        # self.setAllowedAreas(Qt.LeftDockWidgetArea | Qt.RightDockWidgetArea)
        self.layout = QVBoxLayout()

        self.progressbar = QProgressBar()
        self.layout.addWidget(self.progressbar)
        self.label = QtWidgets.QLabel()
        if len(videostreams) > 0:
            width = int(videostreams[0]['width'])
            from tiffy.probe import getstreamtimeinseconds
            seconds = getstreamtimeinseconds(videostreams[0])
            import tempfile
            imgpath = tempfile.gettempdir() + "\\tmp.png"
            import ffmpeg
            (
                ffmpeg
                    .input(path, ss=seconds / 2)
                    .filter('scale', width, -1)
                    .output(imgpath, vframes=1)
                    .run(overwrite_output=True, quiet=True)
            )
            pixmap = QtGui.QPixmap(imgpath)
            pixmap = pixmap.scaled(500, 500, QtCore.Qt.KeepAspectRatio)
            self.label.setPixmap(pixmap)

        self.label.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        self.layout.addWidget(self.label)

        from tiffy.DetailsViewer import DetailsViewer
        self.details = DetailsViewer(path)
        self.layout.addWidget(self.details)

        self.setWindowTitle(path)
        self.setLayout(self.layout)


import threading


class Encoder(threading.Thread):

    def __init__(self, cmd, converter):
        super(Encoder, self).__init__()
        self._stop_event = threading.Event()
        self.process = None
        self.cmd = cmd
        self.converter = converter

    def stop(self):
        self._stop_event.set()

    def stopped(self):
        return self._stop_event.is_set()

    def cleanup(self):
        if self.process is not None:
            def is_running(pid):
                if psutil.pid_exists(pid):
                    return True
                else:
                    return False

            process = self.process
            while is_running(process.pid):
                import time
                time.sleep(0.25)
                print("Kill process", process.pid)
                try:
                    p = psutil.Process(process.pid)
                    p.terminate()
                    p.kill()
                    print("Process killed")
                except psutil.NoSuchProcess:
                    print("Process does not exist anymore")
                    break
        self.process = None

    def __del__(self):
        self.cleanup()

    def run(self):
        cmd = self.cmd
        converter = self.converter
        converter.gobutton.setDisabled(True)
        import ffmpeg
        try:
            from tiffy import probe
            duration = float(ffmpeg.probe(converter.path)['format']['duration'])
            num_frames = int(probe.getAVGFPS(converter.tabstream.videostream.streams[0].stream) * duration)
            width = int(converter.tabstream.videostream.streams[0].stream['width'])
            import subprocess

            self.process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT,
                                            universal_newlines=True)

            for line in self.process.stdout:
                if self.stopped():
                    break
                if "frame=" in line:
                    frameidx = line.find("frame=")
                    fpsidx = line.find("fps=")
                    qidx = line.find("q=")

                    frame = int(line[frameidx + len("frame="):fpsidx])
                    fps = float(line[fpsidx + len("fps="):qidx])

                    progress = int(frame / num_frames * 100.0)
                    converter.datainfo.progressbar.setValue(progress)
                    converter.datainfo.progressbar.setFormat(str(progress) + "% " + str(fps) + "fps")
                    import tempfile
                    imgpath = tempfile.gettempdir() + "\\tmp.png"
                    import ffmpeg
                    (
                        ffmpeg
                            .input(converter.path, ss=frame / num_frames * duration)
                            .filter('scale', width, -1)
                            .output(imgpath, vframes=1)
                            .run(overwrite_output=True, quiet=True)
                    )
                    pixmap = QtGui.QPixmap(imgpath)
                    pixmap = pixmap.scaled(500, 500, QtCore.Qt.KeepAspectRatio)
                    converter.datainfo.label.setPixmap(pixmap)
                    converter.update()
            # converter.killProcesses()
        except ffmpeg.Error as e:
            converter.gobutton.setDisabled(False)
            converter.datainfo.progressbar.setValue(0)
            converter.datainfo.progressbar.setFormat("0%")
            # converter.killProcesses()
            converter.update()
            return -1
        converter.gobutton.setDisabled(False)
        converter.datainfo.progressbar.setValue(100)
        converter.datainfo.progressbar.setFormat("100%")
        # converter.killProcesses()
        converter.update()
        return 0


class Converter(QWidget):
    def __init__(self, path, videostreams, audiostreams):
        QWidget.__init__(self)
        self.layout = QVBoxLayout()

        innerframe = QWidget()

        self.path = path

        from tiffy.VideoStream import VideoStreamSetting

        self.videostreamsetting = VideoStreamSetting(self)

        self.layout.addWidget(self.videostreamsetting)

        from tiffy import TabStream

        self.tabstream = TabStream.TabStream(self, videostreams, audiostreams)
        self.datainfo = DataInfo(path, videostreams)
        self.layout.addWidget(self.tabstream)
        self.layout.addWidget(self.datainfo)

        self.commandline = QLineEdit()

        self.gobutton = QPushButton("GO")
        self.layout.addWidget(self.commandline)
        self.layout.addWidget(self.gobutton)

        self.stopbutton = QPushButton("STOP")
        self.layout.addWidget(self.stopbutton)

        self.setLayout(self.layout)

        # Connecting the signal
        self.gobutton.clicked.connect(self.magic)
        self.stopbutton.clicked.connect(self.stopProcesses)
        self.update_commandline()
        self.thread = None

    def closeEvent(self, event):
        import os
        import signal

        # self.killProcesses()

        event.accept()

    def getffmpeg(self, filename="test-out.mkv"):
        if not hasattr(self, 'tabstream'):
            return None
        import ffmpeg

        i = ffmpeg.input(self.path)

        streams = []

        bitrate = int(self.videostreamsetting.bitrate.bitrate_edit.text()) * 1000

        for stream in self.tabstream.videostream.streams:
            if stream.checkbox.isChecked():
                streams.append(str(stream.stream['index']))

        for stream in self.tabstream.audiostream.streams:
            if stream.checkbox.isChecked():
                streams.append(str(stream.stream['index']))

        outstreams = []
        for j in range(0, len(streams)):
            outstreams.append(i[streams[j]])

        if len(outstreams) == 0:
            return None

        codec = self.videostreamsetting.codec.currentText()

        outfile = None
        if self.videostreamsetting.copycheckbox.isChecked():
            outfile = ffmpeg.output(*outstreams, filename=filename)
            outfile = outfile.global_args('-c:a', 'copy')
            outfile = outfile.global_args('-c:v', 'copy')
            outfile = outfile.global_args('-c:s', 'copy')
        else:
            outfile = ffmpeg.output(*outstreams, filename=filename, video_bitrate=str(bitrate), vcodec=codec)
            outfile = outfile.global_args('-c:a', 'copy')
            outfile = outfile.global_args('-c:s', 'copy')

        return outfile

    @Slot()
    def magic(self):
        self.gobutton.setDisabled(True)
        filename = QFileDialog.getSaveFileName(self, caption="Select output file", dir="../", filter="Matroska (*.mkv)")[
            0]
        import ffmpeg
        ffm = self.getffmpeg(filename=filename)
        if ffm is not None:
            cmd = ffmpeg.compile(ffm)
            cmd.append('-y')
            cmd.append('-hwaccel')
            cmd.append('auto')

            #print(self.videostreamsetting.codec.currentText() in "libx265", self.videostreamsetting.hdrfix is not None, self.videostreamsetting.hdrfix.chkbox.isChecked())

            if (self.videostreamsetting.codec.currentText() in "libx265") and \
                    (self.videostreamsetting.hdrfix is not None) and \
                    (self.videostreamsetting.hdrfix.chkbox.isChecked()):
                #print("asdlkasdalsjdlaskdjlaks")
                libx265opts = str(self.videostreamsetting.hdrfix.line.text())
                #print(libx265opts)
                libx265opts = libx265opts.split(' ')
               # print(libx265opts)
                for opt in libx265opts:
                    cmd.append(opt)

           # print(cmd)
            # import _thread
            # _thread.start_new_thread(encode, (cmd, self))
            if self.thread is not None:
                self.thread.stop()
            self.thread = Encoder(cmd=cmd, converter=self)
            self.thread.start()

        self.gobutton.setDisabled(False)

    @Slot()
    def update_commandline(self):
        if not hasattr(self, 'commandline'):
            return
        import ffmpeg
        ffm = self.getffmpeg()
        if ffm is not None:
            line = ffmpeg.compile(self.getffmpeg())
            self.commandline.setText(str(line))
        else:
            self.commandline.setText('')

    @Slot()
    def stopProcesses(self):
        if self.thread is not None:
            self.thread.stop()
        self.thread = None
        self.gobutton.setDisabled(False)
