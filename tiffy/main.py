import os

from PySide2.QtWidgets import (QVBoxLayout, QWidget, QPushButton, QProgressBar)
from PySide2 import QtCore, QtGui, QtWidgets
from PySide2.QtCore import Slot, Qt
import sys
import qdarkstyle
from PySide2.QtWidgets import QApplication

path = os.getcwd()
os.environ["PATH"] += os.pathsep + path


class UpdateFfmpeg(QWidget):
    def __init__(self, converter):
        QWidget.__init__(self)

        self.converter = converter

        self.layout = QVBoxLayout()

        self.progress_bar = QProgressBar()

        self.layout.addWidget(self.progress_bar)

        self.setLayout(self.layout)

    def closeEvent(self, event):
        self.converter.updater.hide()
        event.accept()


class Tiffy(QWidget):
    def __init__(self):
        QWidget.__init__(self)
        self.setAcceptDrops(True)
        self.frames = []
        pixmap = QtGui.QPixmap('assets/logo.png')
        label = QtWidgets.QLabel()
        pixmap = pixmap.scaled(500, 500, QtCore.Qt.KeepAspectRatio)
        label.setPixmap(pixmap)
        self.layout = QVBoxLayout()

        self.update_ffmpeg = QPushButton("Update ffmpeg")
        # self.layout.addWidget(self.update_ffmpeg)

        self.layout.addWidget(label)
        self.setLayout(self.layout)
        self.update_ffmpeg.clicked.connect(self.startUpdater)

    @Slot()
    def startUpdater(self):
        self.updater = None
        self.updater = UpdateFfmpeg(self)
        self.updater.setWindowModality(QtCore.Qt.ApplicationModal)
        self.updater.show()

    def dragEnterEvent(self, event):
        event.setAccepted(True)

    def dropEvent(self, event):
        import os
        if event.mimeData().hasUrls():
            for url in event.mimeData().urls():
                filepath = url.toLocalFile()

                if os.path.isfile(filepath):
                    from tiffy import probe
                    streams = probe.getstreams(filepath)

                    videostreams = probe.getvideostreams(streams)
                    audiostreams = probe.getaudiostreams(streams)

                    from tiffy.Converter import Converter

                    widget = Converter(filepath, videostreams, audiostreams)
                    widget.resize(800, 600)
                    widget.show()
                    self.frames.append(widget)


def run():
    app = QApplication(sys.argv)
    app.setStyleSheet(qdarkstyle.load_stylesheet_pyside2())
    widget = Tiffy()
    widget.resize(500, 500)
    widget.setWindowTitle("Tiffy - a python gui for ffmpeg")
    widget.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    run()
