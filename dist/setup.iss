#define APPNAME "Tiffy"
#define VERSION "0.1.3"

[Setup]
AppName={#APPNAME}
AppVersion={#VERSION}
AppId={{67070B91-D6B6-42ED-81A7-78059AEB0A72}
OutputBaseFilename={#APPNAME}_Setup_{#VERSION}
WizardStyle=modern
DefaultDirName={autopf}\Tiffy
DefaultGroupName=Tiffy
UninstallDisplayIcon={app}\tiffy.exe
Compression=lzma2
SolidCompression=yes
OutputDir=.\

[Files]
Source: "tiffy\*"; DestDir: "{app}\"; Flags: ignoreversion recursesubdirs
Source: "C:\Program Files\ffmpeg\bin\ffmpeg.exe"; DestDir: "{app}"; Flags: ignoreversion 
Source: "C:\Program Files\ffmpeg\bin\ffplay.exe"; DestDir: "{app}"; Flags: ignoreversion 
Source: "C:\Program Files\ffmpeg\bin\ffprobe.exe"; DestDir: "{app}"; Flags: ignoreversion 

[Icons]
Name: "{group}\Tiffy"; Filename: "{app}\tiffy.exe"
Name: "{commondesktop}\{#APPNAME}"; Filename: "{app}\tiffy.exe";

[Run]
Filename: {app}\tiffy.exe; Description: {cm:LaunchProgram,{#APPNAME}}; Flags: nowait postinstall skipifsilent

[UninstallDelete]
Type: filesandordirs; Name: "{app}\assets"
Type: filesandordirs; Name: "{app}\Include"
Type: filesandordirs; Name: "{app}\lib2to3"
Type: filesandordirs; Name: "{app}\psutil"
Type: filesandordirs; Name: "{app}\PySide2"
Type: filesandordirs; Name: "{app}\shiboken2"
Type: filesandordirs; Name: "{app}\tcl"
Type: filesandordirs; Name: "{app}\tk"
Type: filesandordirs; Name: "{app}\win32com"

